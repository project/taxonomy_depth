MODULE

Taxonomy Depth

DESCRIPTION

The Taxonomy Depth module enables dramatically more efficient taxonomy
"depth" searches than the core taxonomy module.

Drupal vocabularies can be hierarchical; terms can have parents.  A
normal taxonomy search is "retrieve all nodes with term T."  A depth 1
taxonomy search is "retrieve all nodes with term T or any of term T's
children."  A depth N search is "retrieve all nodes with term T or any
of its descendents up to N levels deep."

When Taxonomy module performs a depth D search for a term T, it
retrieves every term in T's vocabulary, scans the results in PHP to
compute the descendant graph dynamically, extracts the descendants of
T of depth D or less, and executes "SELECT FROM node INNER JOIN
term_node WHERE tid IN (all terms in the descendant list)".

When Views module performs a depth D search for a term T, it joins to
the term_hierarchy and term_data tables once for *every* depth level,
plus the initial join to term_node, for a total of 2*D+1 joins.
If you define a page view with three separate Term ID arguments, each
of which does a depth 3 search, you end up with 21 joins in addition
to whatever else the view requires

When Taxonomy Depth module performs a depth D search for a term T, it
joins to the term_node and term_tables once each.  The page view
described above with three depth three searches then only requires 6
joins instead of 21.

STATUS

This module is pre-release.  To do:

* The module should be reworked to provide an API instead of just a
  Views filter and argument.

* The site administrator is warned if a cycle in the taxonomy
  hierarchy is detected but the module does not specify what the cycle
  is even though it has that information handy.

AUTHOR

Barry Jaspan, barry@jaspan.org

SPONSOR

Seven Days Vermont, http://www.7dvt.com
